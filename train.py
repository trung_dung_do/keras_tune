import config
from model import build_model
import utils
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.datasets import fashion_mnist
from tensorflow.keras.utils import to_categorical
from tensorflow.keras import backend as k
from sklearn.metrics import classification_report
import keras_tuner as kt
import numpy as np
import argparse
import cv2


ap = argparse.ArgumentParser()
ap.add_argument('-t', '--tuner', default='hyperband', type=str, choices=['hyperband', 'random', 'bayesian'],
                help='Type of hyperparameter tuner we will be using')
ap.add_argument('-p', '--plot', default='.', help='path to accuracy/loss plot')

args = vars(ap.parse_args())

# load the dataset
((trainX, trainY), (testX, testY)) = fashion_mnist.load_data()

trainX = trainX.reshape((trainX.shape[0], 28, 28, 1))
testX = testX.reshape((testX.shape[0], 28, 28, 1))

trainX = trainX.astype('float32') / 255.
testX = testX.astype('float32') / 255.

trainY = to_categorical(y=trainY, num_classes=10)
testY = to_categorical(y=testY, num_classes=10)

labelNames = ["top", "trouser", "pullover", "dress", "coat",
              "sandal", "shirt", "sneaker", "bag", "ankle boot"]

es = EarlyStopping(monitor='val_loss',
                   patience=config.EARLY_STOPPING_PATIENCE,
                   restore_best_weights=True)

if args['tuner'] == 'hyperband':
    print('[INFO] Initializing a hyperband tuner object...')
    tuner = kt.Hyperband(
        build_model,
        objective='val_accuracy',
        max_epochs=config.EPOCHS,
        factor=3,
        seed=42,
        directory=config.OUTPUT,
        project_name=args['tuner'])
elif args['tuner'] == 'random':
    tuner = kt.RandomSearch(
        build_model,
        objective='val_accuracy',
        max_trials=10,
        seed=42,
        directory=config.OUTPUT,
        project_name=args['tuner']
    )
else:
    tuner = kt.BayesianOptimization(
        build_model,
        objective='val_accuracy',
        max_trials=10,
        seed=42,
        directory=config.OUTPUT,
        project_name=args['tuner']
    )

print('[INFO] perform hyperparameter search...')
tuner.search(x=trainX, y=trainY, validation_data=(testX, testY),
             batch_size=config.BS,
             callbacks=[es],
             epochs=config.EPOCHS)

bestHP = tuner.get_best_hyperparameters(num_trials=1)[0]
print('[INFO] optimal number of filters in conv_1 layer: {}'.format(bestHP.get('conv_1')))
print('[INFO] optimal number of filter in conv_2 layer: {}'.format(bestHP.get('conv_2')))
print('[INFO] optimal number of units in dense layer: {}'.format(bestHP.get('dense_units')))
print('[INFO] optimal value of learning rate: {:.4f}'.format(bestHP.get('learning_rate')))

# build the best model and train it
model = tuner.hypermode.build(bestHP)
H = model.fit(x=trainX, y=trainY, validation_data=(testX, testY),
              batch_size=config.BS,
              epochs=config.EPOCHS,
              callbacks=[es],
              verbose=1)


#evaluate the network
print('[INFO] evaluating the network...')
predictions = model.predict(x=testX, batch_size=32)
print(classification_report(testY.argmax(axis=1), predictions.argmax(axis=1), target_names=labelNames))

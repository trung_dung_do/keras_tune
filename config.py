# define an output directory
OUTPUT = 'output'

# define the input shape for the network
INPUT_SHAPE = (28, 28, 1)

# specify the number of class
NUM_CLASSES = 10

EPOCHS = 50
BS = 32
EARLY_STOPPING_PATIENCE = 5
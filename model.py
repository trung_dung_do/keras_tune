import config

from tensorflow.keras import Sequential
from tensorflow.keras.layers import Conv2D, BatchNormalization, \
    MaxPooling2D, Activation, Flatten, Dropout, Dense
from tensorflow.keras.optimizers import Adam


def build_model(hp):
    model = Sequential()
    chanDim = -1
    inputShape = config.INPUT_SHAPE
    model.add(Conv2D(filters=hp.Int('conv_1', min_value=32, max_value=96, step=32),
                     kernel_size=(3, 3), padding='same',
                     input_shape=inputShape))
    model.add(Activation('relu'))
    model.add(BatchNormalization(axis=chanDim))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(hp.Int('conv_2', min_value=64, max_value=128, step=32),
                     kernel_size=(3, 3), padding='same'))
    model.add(Activation('relu'))
    model.add(BatchNormalization(axis=chanDim))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Flatten())
    model.add(Dense(units=hp.Int(name='dense_units', min_value=256, max_value=768, step=256)))
    model.add(Activation('relu'))
    model.add(BatchNormalization(axis=chanDim))
    model.add(Dropout(0.5))

    model.add(Dense(config.NUM_CLASSES))
    model.add(Activation('softmax'))

    lr = hp.Choice('learning_rate', values=[1e-1, 1e-2, 1e-3])
    opt = Adam(learning_rate=lr)

    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])

    return model
